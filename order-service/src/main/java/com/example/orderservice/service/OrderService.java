package com.example.orderservice.service;

import com.example.model.common.KafkaResponse;
import com.example.model.order.OrderResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderService {
  private final KafkaTemplate<Object, KafkaResponse> kafkaTemplate;

  @Scheduled(fixedDelay = 30000)
  public void sendMessage() {
    int i = 1;
    log.info("send");
    kafkaTemplate.send("order-service", new KafkaResponse(200, new OrderResponseDTO("hello message tu orderservice"))) ;
  }

}
