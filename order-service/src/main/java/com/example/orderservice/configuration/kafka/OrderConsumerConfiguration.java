package com.example.orderservice.configuration.kafka;


import com.example.corekafka.config.kafka.ConsumerConfigBase;
import com.example.model.common.KafkaResponse;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderConsumerConfiguration extends ConsumerConfigBase<KafkaResponse> {
}
