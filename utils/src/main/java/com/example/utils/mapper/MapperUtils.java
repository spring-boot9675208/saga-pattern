package com.example.utils.mapper;

import org.modelmapper.ModelMapper;

public class MapperUtils {
  private static final ModelMapper MODEL_MAPPER = new ModelMapper();

  public static ModelMapper getMapper(){
    return MODEL_MAPPER;
  }
}
