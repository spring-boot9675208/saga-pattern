package com.example.inventoryservice.listener;

import com.example.model.common.KafkaResponse;
import com.example.model.order.OrderResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import static com.example.utils.mapper.MapperUtils.getMapper;


@Component
@Slf4j
@RequiredArgsConstructor
public class InventoryListener {

  @KafkaListener(groupId = "pay-gate",
        topics = "order-service",
        containerFactory = "listenerContainerFactory")
  public void receiveMessage(KafkaResponse message) {
    OrderResponseDTO orderResponseDTO = getMapper().map(message.getData(), OrderResponseDTO.class);
    System.out.println(orderResponseDTO);
  }

}
