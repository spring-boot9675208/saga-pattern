package com.example.inventoryservice.service;

import com.example.inventoryservice.dto.InventoryResponseDTO;
import com.example.model.common.KafkaResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class InventoryService {
  private final KafkaTemplate<Object, KafkaResponse> kafkaTemplate;

  @Scheduled(fixedDelay = 30000)
  public void sendMessage() {
    int i = 1;
    log.info("send");
    kafkaTemplate.send("inventory-service", new KafkaResponse(200, new InventoryResponseDTO("Hello")));
  }

}
