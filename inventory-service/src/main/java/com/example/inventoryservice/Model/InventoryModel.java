package com.example.inventoryservice.Model;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;


@Entity
@Data
@Table(name="inventory")
public class InventoryModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "order_id")
    private Integer orderId;

    @Column(name = "value_payment")
    private Double valuePayment;
}
