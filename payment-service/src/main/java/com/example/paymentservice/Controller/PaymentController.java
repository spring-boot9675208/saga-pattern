package com.example.paymentservice.Controller;

import com.example.model.common.KafkaResponse;
import com.example.paymentservice.listener.OrderListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/payments")
public class PaymentController {
    OrderListener orderListener;
    @GetMapping("/hello")
    @ResponseStatus(HttpStatus.OK)
    @KafkaListener(groupId = "pay-gate",
            topics = "order-service",
            containerFactory = "listenerContainerFactory")
    public ResponseEntity<String> Get_example(KafkaResponse kafkaResponse){
        //String example = "hello";
        String test = kafkaResponse.getData().toString();
        return ResponseEntity.ok(test);
    }
}
