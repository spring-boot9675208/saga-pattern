package com.example.paymentservice.configuration.kafka;

import com.example.corekafka.config.kafka.ProducerConfigBase;
import com.example.model.common.KafkaResponse;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaymentProducerConfiguration extends ProducerConfigBase<KafkaResponse> {
}
