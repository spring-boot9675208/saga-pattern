package com.example.paymentservice.configuration;


import com.example.corekafka.config.service.EnableKafkaServiceCore;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@EnableKafkaServiceCore
public class PaymentServiceConfiguration {


}
