package com.example.paymentservice.listener;

import com.example.model.common.KafkaResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;



@Component
@Slf4j
@RequiredArgsConstructor
public class OrderListener {

  @KafkaListener(groupId = "pay-gate",
        topics = "order-service",
        containerFactory = "listenerContainerFactory")
  public String receiveMessage(KafkaResponse kafkaResponse){
    return kafkaResponse.getData().toString();
  }

}
